package com.example.math_calculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by Steker on 06.03.2015.
 */
public class EndActivity extends Activity {

    TextView tvresult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.end);

        tvresult = (TextView)findViewById(R.id.result);

        // получаем результат из первой активности
        Intent intent = getIntent();
        String resultstr = intent.getStringExtra("resultstr");

        // выводим результат
        tvresult.setText(resultstr);
    }
}